<?php
/**
 * Magebit_ProductComments
 *
 * @category     Magebit
 * @package      Magebit_ProductComments
 * @author       Aigars Bedeicis <aigars.bedeicis@magebit.com>
 * @copyright    Copyright (c) 2018 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\ProductComments\Block;


use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template;

class CommentList extends Template
{
    private $_objectManager;

    public function __construct(Template\Context $context, array $data = [])
    {
        $this->_objectManager = ObjectManager::getInstance();
        parent::__construct($context, $data);
    }

    /**
     * Get products with enabled commenting
     *
     * @return array
     */
    public function getProductsWithComments()
    {
        $products = $this->_objectManager->get('Magento\Catalog\Model\Product')
            ->getCollection()->addAttributeToSelect('commenting')
            ->addAttributeToSelect('name')
            ->addAttributeToFilter(
                array(
                    array('attribute' => 'commenting', 'nonull' => true), // only products with this attribute set
                    array('attribute' => 'commenting', 'eq' => '1') // only products that have it enabled
                ), '', 'left'
            );

        $availableProducts = array();
        foreach ($products as $product) {
            $availableProducts[] = [
                'name' => $product->getName(),
                'comments' => 'TODO',
                'url' => $product->getProductUrl()

            ];
        }

        return $availableProducts;
    }
}