<?php
/**
 * Magebit_ProductComments
 *
 * @category     Magebit
 * @package      Magebit_ProductComments
 * @author       Aigars Bedeicis <aigars.bedeicis@magebit.com>
 * @copyright    Copyright (c) 2018 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\ProductComments\Block;


use Magento\Framework\App\ObjectManager;
use Magento\Framework\View\Element\Template;

class Comment extends Template
{
    protected $_registry;
    protected $_objectManager;

    public function __construct(
        Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_objectManager = ObjectManager::getInstance();
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }

    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

    /**
     * Get Current Product
     *
     * @return mixed
     */
    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    /**
     * Check if comments are disabled for current product
     *
     * @return
     */
    public function isCommentsEnabled()
    {
        //echo ($this->getData('commenting')) ? 'true' : 'false';
        return $this->getCurrentProduct()->getData('commenting');
        /* if ($product->get)*/
    }

    /**
     * Get all approved product comments
     *
     * @return array
     */
    public function getComments()
    {

        $comments = $this->_objectManager->get('Magebit\ProductComments\Model\Comment')->getCollection()
            ->addFieldToFilter('product_id', array('eq' => $this->getCurrentProduct()->getId()))
            ->addFieldToFilter('comment_status', array('eq' => 1))
            ->getItems();
        //print_r(get_class_methods($this->_objectManager->get('Magebit\ProductComments\Model\Comment')->getCollection()));
        $list = array();
        foreach ($comments as $item) {
            $list[] = [
                'text' => $item->getCommentText(),
                'author' => $item->getCommentName(),
                'date' => $item->getCommentDate(),
                'email' => $item->getCommentEmail()
            ];
        }
        return $list;
        //print_r('hello');
        //print_r(get_class_methods($this->getCurrentProduct()));
        //print_r(get_class_methods($this));
    }

    public function getFormAction()
    {
        return $this->getUrl('productcomments/comment/new', ['_secure' => true]);
    }
}