<?php
/**
 * Magebit_ProductComments
 *
 * @category     Magebit
 * @package      Magebit_ProductComments
 * @author       Aigars Bedeicis <aigars.bedeicis@magebit.com>
 * @copyright    Copyright (c) 2018 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\ProductComments\Block\Adminhtml;


class Comment extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_comment';
        $this->_blockGroup = 'Magebit_ProductComments';
        $this->_headerText = __('Product Comments');
        $this->_addButtonLabel = __('Create New Comment');
        parent::_construct();
    }
}