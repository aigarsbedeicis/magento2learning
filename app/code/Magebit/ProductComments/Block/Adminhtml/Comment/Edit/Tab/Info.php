<?php
/**
 * Magebit_ProductComments
 *
 * @category     Magebit
 * @package      Magebit_ProductComments
 * @author       Aigars Bedeicis <aigars.bedeicis@magebit.com>
 * @copyright    Copyright (c) 2018 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\ProductComments\Block\Adminhtml\Comment\Edit\Tab;


use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Model\Auth\Session;

class Info extends Generic implements TabInterface
{
    protected $_auth;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Backend\Model\Auth $auth,
        array $data = []
    ) {
        $this->_auth = $auth;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareForm()
    {


        $model = $this->_coreRegistry->registry('productcomments_comment');
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('comment_');
        $form->setFieldNameSuffix('comment');

        $data = $model->getData();
        $new = false;
        if (empty($data)) {
            $new = true;
            $admin = $this->_auth->getUser();
            $data['comment_name'] = $admin->getFirstName() . ' ' . $admin->getLastName();
            $data['comment_email'] = $admin->getEmail();
        }

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General')]
        );
        if ($model->getCommentId()) {
            $fieldset->addField(
                'comment_id',
                'hidden',
                [
                    'name' => 'comment_id'
                ]
            );
        }
        $fieldset->addField(
            'product_id',
            'text',
            [
                'name' => 'product_id',
                'label' => __('Product Id'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'comment_email',
            'hidden',
            ['name' => 'comment_email']
        );

        $fieldset->addField(
            'comment_name',
            'text',
            [
                'name' => 'comment_name',
                'readonly' => $new,
                'label' => __('Customer Name'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'comment_text',
            'textarea',
            [
                'name' => 'comment_text',
                'label' => __('Comment'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'comment_status',
            'select',
            [
                'name' => 'comment_status',
                'label' => __('Comment Status'),
                'required' => true,
                'options' => [ '1' => __('Approved'), '0' => __('Not Approved')],
                'default' => '0',
                'prefer' => 'toggle'
            ]

        );


        $form->setValues($data);
        $this->setForm($form);
        return parent::_prepareForm();
    }
    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return __('Comment Info');
    }
    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return __('Comment Info');
    }
    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }
    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }


}