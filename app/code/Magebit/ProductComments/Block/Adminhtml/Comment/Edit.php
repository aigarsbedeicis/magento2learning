<?php
/**
 * Magebit_ProductComments
 *
 * @category     Magebit
 * @package      Magebit_ProductComments
 * @author       Aigars Bedeicis <aigars.bedeicis@magebit.com>
 * @copyright    Copyright (c) 2018 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\ProductComments\Block\Adminhtml\Comment;


use Magento\Framework\Registry;
use Magento\Backend\Block\Widget\Context;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    protected $_coreRegistry;

    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->_objectId = 'comment_id';
        $this->_controller = 'adminhtml_comment';
        $this->_blockGroup = 'Magebit_ProductComments';
        parent::_construct();
        $this->buttonList->update('save', 'label', __('Save'));
        $this->buttonList->add('saveandcontinue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'saveAndContinueEdit',
                            'target' => '#edit_form'

                        ]
                    ]
                ]
            ],
            -100
        );
        $this->buttonList->add('delete',
            [
                'label' => __('Delete'),
                'class' => 'delete',
                'onclick' => 'deleteConfirm(' . json_encode(__('Are you sure you want this comment to be deleted?')) . ','
                    . json_encode($this->getDeleteUrl()) . ')',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'delete',
                            'target' => '#edit_form'
                        ]
                    ]
                ]
            ]
        );
        $this->buttonList->remove('reset');
    }

    public function getHeaderText()
    {
        $commentRegistry = $this->_coreRegistry->registry('productcomments_comment');
        if ($commentRegistry->getId()) {
            $commentTitle = $this->escapeHtml($commentRegistry->getTitle());
            return __("Edit Comment '%1'", $commentTitle);
        } else {
            return __('Add Comment');
        }
    }

    protected function _prepareLayout()
    {
        $this->_formScripts[] = "
            function toggleEditor() {
            if (tinyMCE.getInstanceById('post_content') == null) {
            tinyMCE.execCommand('mceAddControl', false, 'post_content');
            } else {
            tinyMCE.execCommand('mceRemoveControl', false, 'post_content');
            }
            };
            ";
        return parent::_prepareLayout();
    }

}