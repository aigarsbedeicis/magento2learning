<?php
/**
 * Magebit_ProductComments
 *
 * @category     Magebit
 * @package      Magebit_ProductComments
 * @author       Aigars Bedeicis <aigars.bedeicis@magebit.com>
 * @copyright    Copyright (c) 2018 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\ProductComments\Controller\Comment;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\PageFactory;
use Zend\Validator\EmailAddress;

class NewAction extends Action
{
    protected $_pageFactory;

    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    public function saveComment($name, $email, $text, $productId)
    {
        $item = $this->_objectManager->create('Magebit\ProductComments\Model\Comment');
        $item->setCommentName($name);
        $item->setCommentEmail($email);
        $item->setCommentText($text);
        $item->setCommentStatus(0);
        $item->setProductId($productId);
        $item->save();
    }

    public function execute()
    {

        if ($this->getRequest()->isPost()) {
            try {
                $email = $this->getRequest()->getPost('email');
                $name = $this->getRequest()->getPost('name');
                $text = $this->getRequest()->getPost('text');
                $productId = $this->getRequest()->getPost('productId');
                if (!((new EmailAddress())->isValid($email))) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        (__("Invalid Email!"))
                    );
                }
                $validator = new \Zend\I18n\Validator\Alnum(array('allowWhiteSpace' => true));
                if (!$validator->isValid($name)) {
                    throw new LocalizedException(__("Invalid Name"));
                } elseif (!$validator->isValid($text)) {
                    throw new LocalizedException(__("Invalid text"));
                }

                $this->saveComment($name, $email, $text, $productId);
                $this->messageManager->addSuccess(__('Comment has been saved and awaits approval.'));

            } catch (LocalizedException $e) {
                $this->messageManager->addException(
                    $e,
                    __('%1', $e->getMessage())
                );
            } catch (\Zend_Validate_Exception $e) {
                $this->messageManager->addException(
                    $e,
                    __('%1', $e->getMessage())
                );
            }
        }
        // else
        // {
        $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl());
        //}
        //return $this->_pageFactory->create();
    }
}