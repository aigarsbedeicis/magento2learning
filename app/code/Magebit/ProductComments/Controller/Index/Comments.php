<?php
/**
 * Created by PhpStorm.
 * User: magebit
 * Date: 18.26.9
 * Time: 13:24
 */

namespace Magebit\ProductComments\Controller\Index;


use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;

class Comments extends Action
{
    public function execute()
    {
        $post = (array)$this->getRequest()->getPost();
        if (!empty($post)) {

            $this->messageManager->addSuccessMessage("Yaaay");

            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl('/productcomments/index/comment');
        }

        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}