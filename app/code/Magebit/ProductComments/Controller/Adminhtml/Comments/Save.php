<?php
/**
 * Magebit_ProductComments
 *
 * @category     Magebit
 * @package      Magebit_ProductComments
 * @author       Aigars Bedeicis <aigars.bedeicis@magebit.com>
 * @copyright    Copyright (c) 2018 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\ProductComments\Controller\Adminhtml\Comments;


use Magento\Framework\Exception\LocalizedException;
use Zend\Validator\EmailAddress;

class Save extends Index
{
    public function execute()
    {
        $isPost = $this->getRequest()->getPost();
        if ($isPost) {
            $commentModel = $this->_commentFactory->create();
            $commentId = $this->getRequest()->getParam('comment_id');
            if ($commentId) {
                $commentModel->load($commentId);
            }
            $formData = $this->getRequest()->getParam('comment');
            $commentModel->setData($formData);
            try {
                $product = $this->_objectManager->create(\Magento\Catalog\Model\Product::class)->load($formData['product_id']);
                if (!$product->getId()) {
                    throw new LocalizedException(__('Product with this ID does not exist!'));
                }

                if (!((new EmailAddress())->isValid($formData['comment_email']))) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        (__("Invalid Email!"))
                    );
                }
                $validator = new \Zend\I18n\Validator\Alnum(array('allowWhiteSpace' => true));
                if (!$validator->isValid($formData['comment_name'])) {
                    throw new LocalizedException(__("Invalid Name"));
                } elseif (!$validator->isValid($formData['comment_text'])) {
                    throw new LocalizedException(__("Invalid text"));
                }

                $commentModel->save();
                // Display success message
                $this->messageManager->addSuccess(__('The comment has been saved.'));
                // Check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['comment_id' => $commentModel->getId(), '_current' => true]);
                    return;
                }
                // Go to grid page
                $this->_redirect('*/*/');
                return;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
            $this->_getSession()->setFormData($formData);
            $this->_redirect('*/*/edit', ['comment_id' => $commentId]);
        }
    }

}