<?php
/**
 * Magebit_ProductComments
 *
 * @category     Magebit
 * @package      Magebit_ProductComments
 * @author       Aigars Bedeicis <aigars.bedeicis@magebit.com>
 * @copyright    Copyright (c) 2018 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\ProductComments\Controller\Adminhtml\Comments;


class Delete extends Index
{
    public function execute()
    {
        $commentId = (int)$this->getRequest()->getParam('comment_id');
        if ($commentId) {
            $commentModel = $this->_commentFactory->create();
            $commentModel->load($commentId);
            // Check this news exists or not
            if (!$commentModel->getId()) {
                $this->messageManager->addError(__('This comment no longer exists.'));
            } else {
                try {
                    // Delete news
                    $commentModel->delete();
                    $this->messageManager->addSuccess(__('The comment has been deleted.'));
                    // Redirect to grid page
                    $this->_redirect('*/*/');
                    return;
                } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());
                    $this->_redirect('*/*/edit', ['comment_id' => $commentModel->getCommentId()]);
                }
            }
        }
    }
}