<?php
/**
 * Magebit_ProductComments
 *
 * @category     Magebit
 * @package      Magebit_ProductComments
 * @author       Aigars Bedeicis <aigars.bedeicis@magebit.com>
 * @copyright    Copyright (c) 2018 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\ProductComments\Controller\Adminhtml\Comments;


class MassDelete extends \Magento\Backend\App\Action
{

    public function execute()
    {
        $commentIds = $this->getRequest()->getParam('comment');

        if (!is_array($commentIds)) {
            $this->messageManager->addError(__('Please select one or more comments.'));
        } else {
            try {
                foreach ($commentIds as $commentId) {
                    $comment = $this->_objectManager->create(\Magebit\ProductComments\Model\Comment::class)->load($commentId);
                    $comment->delete();
                }
                $this->messageManager->addSuccess(__('%1 comment(s) were deleted', count($commentIds)));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        $this->_redirect("*/*/index");
    }

}