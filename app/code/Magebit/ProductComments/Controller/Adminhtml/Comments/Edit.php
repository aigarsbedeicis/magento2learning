<?php
/**
 * Magebit_ProductComments
 *
 * @category     Magebit
 * @package      Magebit_ProductComments
 * @author       Aigars Bedeicis <aigars.bedeicis@magebit.com>
 * @copyright    Copyright (c) 2018 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\ProductComments\Controller\Adminhtml\Comments;


use Magento\Backend\App\Action;

class Edit extends Index
{
    public function execute()
    {
        $commentId = $this->getRequest()->getParam('comment_id');
        $model = $this->_commentFactory->create();
        if ($commentId) {
            $model->load($commentId);
            if (!$model->getCommentId()) {
                $this->messageManager->addError(__('This comment no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }
        // Restore previously entered form data from session
        $data = $this->_session->getCommentData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->_coreRegistry->register('productcomments_comment', $model);
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Magebit_ProductComment::main_menu');
        $resultPage->getConfig()->getTitle()->prepend(__('Product Comment'));
        return $resultPage;
    }

}