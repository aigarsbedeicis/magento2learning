<?php
/**
 * Magebit_ProductComments
 *
 * @category     Magebit
 * @package      Magebit_ProductComments
 * @author       Aigars Bedeicis <aigars.bedeicis@magebit.com>
 * @copyright    Copyright (c) 2018 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\ProductComments\Controller\Adminhtml\Comments;

use Magebit\ProductComments\Model\CommentFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;

class Index extends Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    protected $_commentFactory;

    protected $_productFactory;

    protected $_coreRegistry;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        Registry $registry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        CommentFactory $commentFactory
    ) {

        parent::__construct($context);
        $this->_coreRegistry = $registry;
        $this->_commentFactory = $commentFactory;
        $this->_resultPageFactory = $resultPageFactory;
    }

    /**
     * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Product Comments'));
        return $resultPage;
    }

}