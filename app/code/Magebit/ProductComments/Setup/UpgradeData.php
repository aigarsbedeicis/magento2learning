<?php
/**
 * Created by PhpStorm.
 * User: magebit
 * Date: 18.26.9
 * Time: 11:05
 */

namespace Magebit\ProductComments\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Cms\Model\PageFactory $pageFactory
    ) {
        $this->_pageFactory = $pageFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $page = $this->_pageFactory->create();
            $page->setTitle("Comment List")
                ->setIdentifier('comment_list')
                ->setIsActive(true)
                ->setPageLayout('1column')
                ->setStores(array(0))
                ->setContent('{{block class="Magebit\ProductComments\Block\CommentList" name="commentlist" template="Magebit_ProductComments::commentlist.phtml"}}')
                ->save();

            $setup->endSetup();
        }
    }
}