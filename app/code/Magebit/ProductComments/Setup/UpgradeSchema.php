<?php
/**
 * Created by PhpStorm.
 * User: magebit
 * Date: 18.26.9
 * Time: 10:36
 */

namespace Magebit\ProductComments\Setup;


use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // this should be in InstallSchema
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            if (!$setup->tableExists('magebit_product_comments')) {
                $table = $setup->getConnection()->newTable($setup->getTable('magebit_product_comments'))
                    ->addColumn(
                        'comment_id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'primary' => true,
                            'nullable' => false,
                            'unsigned' => true,
                        ],
                        'Comment ID'
                    )
                    ->addColumn(
                        'comment_email',
                        Table::TYPE_TEXT,
                        40,
                        [
                            'nullable' => false
                        ],
                        'Email'
                    )
                    ->addColumn(
                        'comment_text',
                        Table::TYPE_TEXT,
                        400,
                        [
                            'nullable' => false
                        ],
                        'Text'
                    )
                    ->addColumn(
                        'comment_date',
                        Table::TYPE_TIMESTAMP,
                        null,
                        [
                            'nullable' => false,
                            'default' => Table::TIMESTAMP_INIT,
                        ],
                        'Date'
                    )
                    ->addColumn(
                        'comment_status',
                        Table::TYPE_BOOLEAN,
                        null,
                        [
                            'nullable' => false,
                        ],
                        'Comment Status'
                    )
                    ->addColumn(
                        'product_id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'unsigned' => true,
                            'nullable' => false
                        ],
                        'Product ID'
                    )
                    ->addForeignKey(
                        $setup->getFkName(
                            $setup->getTable('magebit_product_comments'),
                            'product_id',
                            $setup->getTable('catalog_product_entity'),
                            'entity_id'
                        ),
                        'product_id',
                        $setup->getTable('catalog_product_entity'),
                        'entity_id',
                        Table::ACTION_CASCADE
                    );
                $setup->getConnection()->createTable($table);

                $setup->getConnection()->addIndex(
                    $setup->getTable('magebit_product_comments'),
                    $setup->getIdxName(
                        $setup->getTable('magebit_product_comments'),
                        ['comment_email', 'comment_text'],
                        AdapterInterface::INDEX_TYPE_FULLTEXT
                    ),
                    ['comment_email', 'comment_text'],
                    AdapterInterface::INDEX_TYPE_FULLTEXT
                );
            }
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $table = $setup->getTable('magebit_product_comments');
            $setup->getConnection()->addColumn(
                $table,
                'comment_name',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 50,
                    'nullable' => false,
                    'comment' => 'Name'
                ]
            );
        }
        $setup->endSetup();
    }
}