<?php
/**
 * Created by PhpStorm.
 * User: magebit
 * Date: 18.26.9
 * Time: 11:56
 */

namespace Magebit\ProductComments\Model;


use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Comment extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'magebit_productcomments_comment';

    public function _construct()
    {
        $this->_init(\Magebit\ProductComments\Model\ResourceModel\Comment::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

}