<?php
/**
 * Created by PhpStorm.
 * User: magebit
 * Date: 18.26.9
 * Time: 12:06
 */

namespace Magebit\ProductComments\Model\ResourceModel\Comment;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'comment_id';
    protected $_eventPrefix = 'magebit_productcomments_comment_collection';
    protected $_eventObject = 'comment_collection';
    protected $_objectManager;

    protected function _construct()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_init('Magebit\ProductComments\Model\Comment', 'Magebit\ProductComments\Model\ResourceModel\Comment');
    }

    protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()->joinLeft(
            ['product_table' => $this->getTable('catalog_product_entity')],
            'main_table.product_id = product_table.entity_id',
            ['entity_id']
        ); // join product_id in our table with product entity

        $productNameId = $this->_objectManager->create('Magento\Eav\Model\Config')
            ->getAttribute(\Magento\Catalog\Model\Product::ENTITY, \Magento\Catalog\Api\Data\ProductInterface::NAME)
            ->getAttributeId();

        $this->getSelect()->joinLeft(
            ['product_varchar' => $this->getTable('catalog_product_entity_varchar')],
            'product_varchar.entity_id = product_table.entity_id AND product_varchar.attribute_id = ' . $productNameId,
            [])
            ->columns(['product_name' => 'product_varchar.value']); // get name

        $this->getSelect()->group('comment_id'); // remove duplicates

        return $this;
    }
}