<?php
/**
 * Created by PhpStorm.
 * User: magebit
 * Date: 18.26.9
 * Time: 12:02
 */

namespace Magebit\ProductComments\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Comment extends AbstractDb
{
    public function _construct()
    {
        $this->_init('magebit_product_comments', 'comment_id');
    }

}