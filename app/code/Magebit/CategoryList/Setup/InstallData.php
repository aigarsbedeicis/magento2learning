<?php
/**
 * Created by PhpStorm.
 * User: magebit
 * Date: 18.25.9
 * Time: 08:53
 */

namespace Magebit\CategoryList\Setup;


use Magento\Cms\Model\BlockFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $blockFactory;

    public function __construct(BlockFactory $blockFactory)
    {
        $this->blockFactory = $blockFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $cmsBlock = [
            'title' => 'Category List Test',
            'identifier' => 'categorylist/list_test',
            'is_active' => 1,
            'stores' => [0],
            'sort_order' => 0,
            'content' => "<h1>hello</h1>"
        ];

        $this->blockFactory->create()->setData($cmsBlock)->save();
    }

}