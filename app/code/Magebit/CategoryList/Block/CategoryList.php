<?php
/**
 * Created by PhpStorm.
 * User: magebit
 * Date: 18.25.9
 * Time: 09:32
 */

namespace Magebit\CategoryList\Block;


use Magento\Framework\View\Element\Template;

class CategoryList extends \Magento\Framework\View\Element\Template
{
    private $_objectManager;

    public function __construct(Template\Context $context, array $data = [])
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct($context, $data);
    }

    public function hello()
    {
        return __('hello');
    }

    public function getCategory($categoryId)
    {
        return $this->_objectManager->create('Magento\Catalog\Model\Category')->load($categoryId);

    }

    public function isCategoryValid()
    {
        $cat = $this->getCategory($this->getCategoryId());
        if ($cat->getId()) {
            return true;
        }
        return false;
    }

    public function getCategoryInfo()
    {
        $category_id = $this->getCategoryId();
        $category = $this->getCategory($category_id);

        //print_r(get_class_methods($category));
        return [
            'name' => $category->getName(),
            'url' => $category->getUrl(),
            'productCount' => $category->getProductCount(),
        ];

    }
    public function getSubCategories()
    {
        $category_id = $this->getCategoryId();
        $category = $this->getCategory($category_id);
        $subCategories = $category->getChildrenCategories()->getItems();
        $subCat = array();
        foreach ($subCategories as $key=>$cat)
        {
            $subCat[$key] = [
                'name' => $cat->getName(),
                'url' => $cat->getUrl(),
                'productCount' => $cat->getProductCount()
            ];
        }
        return $subCat;
    }
}