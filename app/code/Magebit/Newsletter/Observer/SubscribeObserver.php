<?php
/**
 * Created by PhpStorm.
 * User: magebit
 * Date: 18.20.9
 * Time: 16:03
 */

namespace Magebit\Newsletter\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class SubscribeObserver implements ObserverInterface
{

    public function execute(Observer $observer)
    {
        $collection = $observer->getData('collection');
        $grid = $observer->getData('grid');
        if ($grid instanceof \Magento\Newsletter\Block\Adminhtml\Subscriber\Grid) // only add it to Newsletter/Subscriber table
        {
            $addedColumn = [
               'header' => 'Name',
               'index' => 'name',
               'type' => 'text',
                'default' => '----'
            ];
            // newsletter subscriber doesnt implement Extended Column class and its addColumn method
            $grid->getColumnSet()->setChild( "name",
                $grid->getLayout()
                    ->createBlock(\Magento\Backend\Block\Widget\Grid\Column\Extended::class)
                    ->setData($addedColumn)
                    ->setId("name")
                    ->setGrid($grid)
            );
            $grid->getColumnSet()->getChildBlock("name")->setGrid($grid);
            $collection->addFilterToMap("name","main_table.name"); // name is in some other table too
        }
    }

}