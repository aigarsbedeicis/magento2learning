<?php
/**
 * Created by PhpStorm.
 * User: magebit
 * Date: 18.19.9
 * Time: 15:32
 */

namespace Magebit\Newsletter\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;


class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // runs on module upgrade bin/magento setup:upgrade or schema upgrade
        $version = $context->getVersion(); // previous version of module
        if (!$version) {
            // first time running this
            // InstallSchema already did all work needed
            return;
        }
        $setup->startSetup();
        if (version_compare($version,'1.0.6') < 0) {
            // previous version was before 1.0.4
            $tableName = $setup->getTable('newsletter_subscriber');
            $setup->getConnection()->addColumn(
                $tableName,
                'name',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 50,
                    'nullable' => false,
                    'comment' => 'Name'
                ]
            );
        }
        $setup->endSetup();
    }
}