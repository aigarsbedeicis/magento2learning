<?php
/**
 * Created by PhpStorm.
 * User: magebit
 * Date: 18.20.9
 * Time: 13:08
 */

namespace Magebit\Newsletter\Controller\Subscriber;


class NewAction extends \Magento\Newsletter\Controller\Subscriber\NewAction
{
    public function execute()
    {
        //$this->messageManager->addSuccess($this->getRequest()->getPost('name'));
        if ($this->getRequest()->isPost())
        {

            try {
                $email = (string)$this->getRequest()->getPost('email');
                $name = (string)$this->getRequest()->getPost('name');
                if (!$name)
                {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        (__("No Name was provided."))
                    );
                } elseif (!$email)
                {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        (__("No E-Mail was provided."))
                    );
                }
                $this->validateEmailFormat($email);
                $this->validateGuestSubscription();
                $this->validateEmailAvailable($email);

                $subscriber = $this->_subscriberFactory->create()->loadByEmail($email);
                if ($subscriber->getId()
                    && $subscriber->getSubscriberStatus() == \Magento\Newsletter\Model\Subscriber::STATUS_SUBSCRIBED
                ) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __('This email address is already subscribed.')
                    );
                }
                //$email .= "testing";
                $status = $this->_subscriberFactory->create()->customSubscribe($email,$name); // creates subscription
                if ($status == \Magento\Newsletter\Model\Subscriber::STATUS_NOT_ACTIVE) {
                    $this->messageManager->addSuccess(__('The confirmation request has been sent.'));
                } else {
                    $this->messageManager->addSuccess(__('Thank you for your subscription.'));
                }
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addException(
                    $e,
                    __('There was a problem with the subscription: %1', $e->getMessage())
                );
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong with the subscription: %1.',$e->getMessage()));
            }
        }
        $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl());
    }
}