#!/bin/bash

cd /var/www/magento2-test/public
rm -R var/view_preprocessed/*
rm -R pub/static/*

bin/magento cache:flush
